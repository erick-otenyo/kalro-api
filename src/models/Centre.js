import { autoIncrement } from "mongoose-plugin-autoinc";

var mongoose = require("mongoose"),
  uniqueValidator = require("mongoose-unique-validator");

var productSubSchema = mongoose.Schema(
  {
    type: String,
    items: [String]
  },
  { _id: false }
);

var CentreSchema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    admin: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    category: String,
    description: String,
    contacts: String,
    products: [productSubSchema],
    county: String,
    coordinates: Array
  },
  { timestamps: true }
);

CentreSchema.plugin(uniqueValidator, { message: "is already taken" });

CentreSchema.plugin(autoIncrement, {
  model: "Centre",
  startAt: 1
});

CentreSchema.methods.togeoJSONFor = function(user) {
  return {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: this.coordinates
    },
    id: this._id,
    properties: {
      name: this.name,
      admin:this.admin,
      category: this.category,
      contacts: this.contacts,
      description: this.description,
      county: this.county
    }
  };
};

mongoose.model("Centre", CentreSchema);
