import { autoIncrement } from "mongoose-plugin-autoinc";

var mongoose = require("mongoose"),
  uniqueValidator = require("mongoose-unique-validator");

var InstituteSchema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    admin: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
    category: String,
    institute: String,
    description: String,
    county: String,
    coordinates: Array
  },
  { timestamps: true }
);

InstituteSchema.plugin(uniqueValidator, { message: "is already taken" });

InstituteSchema.plugin(autoIncrement, {
  model: "Insitute",
  startAt: 1
});

InstituteSchema.methods.togeoJSONFor = function(user) {
  return {
    type: "Feature",
    geometry: {
      type: "Point",
      coordinates: this.coordinates
    },
    id: this._id,
    properties: {
      name: this.name,
      category: this.category,
      description: this.description,
      county: this.county,
      institute: this.institute
    }
  };
};

mongoose.model("Institute", InstituteSchema);
