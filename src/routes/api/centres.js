var router = require("express").Router();
var mongoose = require("mongoose");
var Centre = mongoose.model("Centre");
var User = mongoose.model("User");
var auth = require("../auth");

var Promise = global.Promise;

// Preload centre objects on routes with ':centre'
router.param("centre", function(req, res, next, id) {
  Centre.findOne({ _id: id })
    .then(function(centre) {
      if (!centre) {
        return res.sendStatus(404);
      }
      req.centre = centre;
      return next();
    })
    .catch(next);
});

// get all centres
router.get("/", auth.optional, function(req, res, next) {
  var query = {};
  var limit = 10;
  var offset = 0;
  Promise.all([
    Centre.find(query)
      .limit(Number(limit))
      .skip(Number(offset))
      .sort({ createdAt: "desc" })
      .exec(),
    Centre.count(query).exec()
  ])
    .then(function(results) {
      var centres = results[0];
      var centresCount = results[1];

      return res.json({
        centres: centres.map(function(centre) {
          return centre.togeoJSONFor();
        }),
        centresCount: centresCount
      });
    })
    .catch(next);
});

// create new centre
// router.post("/", auth.required, function(req, res, next) {
//   User.findById(req.payload.id)
//     .then(function(user) {
//       if (!user) {
//         return res.sendStatus(401);
//       }
//       var centre = new Centre(req.body.centre);

//       centre.admin = user;

//       return centre.save().then(function() {
//         return res.json({ centre: centre.togeoJSONFor(user) });
//       });
//     })
//     .catch(next);
// });

router.post("/", auth.required, function(req, res, next) {
  User.findById(req.payload.id)
    .then(function(user) {
      if (!user) {
        return res.sendStatus(401);
      }
      var centres = req.body.centres.map(centre => {
        const centre_ = {
          admin: new mongoose.Types.ObjectId(user._id),
          name: centre.properties.NAME,
          category: centre.properties.CATEGORY,
          county: centre.properties.COUNTY,
          coordinates: centre.geometry.coordinates
        };
        return centre_;
      });

      return Centre.insertMany(centres).then(function(docs) {
        console.log(docs);
        return res.json({
          centres: {
            type: "FeatureCollection",
            features: docs.map(doc => {
              return doc.togeoJSONFor();
            })
          }
        });
      });
    })
    .catch(next);
});

// return a centre
router.get("/:centre", auth.optional, function(req, res, next) {
  Promise.all([
    req.payload ? User.findById(req.payload.id) : null,
    req.centre.populate("admin").execPopulate()
  ])
    .then(function(results) {
      var user = results[0];

      return res.json({ centre: req.centre.togeoJSONFor(user) });
    })
    .catch(next);
});

// update centre
router.put("/:centre", auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(admin) {
    if (req.centre.admin._id.toString() === req.payload.id.toString()) {
      // admin: { type: mongoose.Types.ObjectId, ref: "User" },
      // category: String,
      // description: String,
      // county: String,
      // coordinates:
      if (typeof req.body.centre.name !== "undefined") {
        req.centre.name = req.body.centre.name;
      }
      if (typeof req.body.centre.category !== "undefined") {
        req.centre.category = req.body.centre.category;
      }
      if (typeof req.body.centre.description !== "undefined") {
        req.centre.description = req.body.centre.description;
      }
      if (typeof req.body.centre.coordinates !== "undefined") {
        req.centre.coordinates = req.body.centre.coordinates;
      }
      if (typeof req.body.centre.county !== "undefined") {
        req.centre.county = req.body.centre.county;
      }
      if (typeof req.body.centre.products !== "undefined") {
        req.centre.products = req.body.centre.products;
      }

      req.centre
        .save()
        .then(function(centre) {
          return res.json({ centre: centre.togeoJSONFor(admin) });
        })
        .catch(next);
    } else {
      return res.sendStatus(403);
    }
  });
});

// delete centre
router.delete("/:centre", auth.required, function(req, res, next) {
  User.findById(req.payload.id)
    .then(function(user) {
      if (!user) {
        return res.sendStatus(401);
      }

      if (req.centre.admin._id.toString() === req.payload.id.toString()) {
        return req.centre.remove().then(function() {
          return res.sendStatus(204);
        });
      } else {
        return res.sendStatus(403);
      }
    })
    .catch(next);
});
// return a centre's tasks
// router.get("/:centre/tasks", auth.optional, function(req, res, next) {
//   Promise.resolve(req.payload ? User.findById(req.payload.id) : null)
//     .then(function(user) {
//       return Promise.all([
//         Task.find({
//           centre: req.params.centre
//         })
//           .sort({ createdAt: "desc" })
//           .exec(),
//         Task.count({
//           centre: req.params.centre
//         }).exec()
//       ]).then(function(results) {
//         var tasks = results[0];
//         var tasksCount = results[1];
//         return res.json({
//           tasks: tasks.map(function(task) {
//             return task.toJSONFor();
//           }),
//           tasksCount: tasksCount
//         });
//       });
//     })
//     .catch(next);
// });
module.exports = router;
