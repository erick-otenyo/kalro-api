var router = require("express").Router();
var mongoose = require("mongoose");
var Institute = mongoose.model("Institute");
var User = mongoose.model("User");
var auth = require("../auth");

var Promise = global.Promise;

// Preload institute objects on routes with ':institute'
router.param("institute", function(req, res, next, id) {
  Institute.findOne({ _id: id })
    .then(function(institute) {
      if (!institute) {
        return res.sendStatus(404);
      }

      req.institute = institute;
      return next();
    })
    .catch(next);
});

// get all institutes
router.get("/", auth.optional, function(req, res, next) {
  var query = {};
  var limit = 10;
  var offset = 0;
  Promise.all([
    Institute.find(query)
      .limit(Number(limit))
      .skip(Number(offset))
      .sort({ createdAt: "desc" })
      .exec(),
    Institute.count(query).exec()
  ])
    .then(function(results) {
      var institutes = results[0];
      var institutesCount = results[1];

      return res.json({
        institutes: institutes.map(function(institute) {
          return institute.togeoJSONFor();
        }),
        institutesCount: institutesCount
      });
    })
    .catch(next);
});

// create new institute
// router.post("/", auth.required, function(req, res, next) {
//   User.findById(req.payload.id)
//     .then(function(user) {
//       if (!user) {
//         return res.sendStatus(401);
//       }
//       var institute = new Institute(req.body.institute);

//       institute.admin = user;

//       return institute.save().then(function() {
//         return res.json({ institute: institute.togeoJSONFor(user) });
//       });
//     })
//     .catch(next);
// });

router.post("/", auth.required, function(req, res, next) {
  User.findById(req.payload.id)
    .then(function(user) {
      if (!user) {
        return res.sendStatus(401);
      }

      var institutes = req.body.institutes.map(institute => {
        const institute_ = {
          admin: new mongoose.Types.ObjectId(user._id),
          name: institute.properties.NAME,
          category: institute.properties.CATEGORY,
          institute: institute.properties.INSTITUTE,
          county: institute.properties.COUNTY,
          coordinates: institute.geometry.coordinates
        };
        return institute_;
      });

      return Institute.insertMany(institutes).then(function(docs) {
        console.log(docs);
        return res.json({
          institutes: {
            type: "FeatureCollection",
            features: docs.map(doc => {
              return doc.togeoJSONFor();
            })
          }
        });
      });
    })
    .catch(next);
});

// return a institute
router.get("/:institute", auth.optional, function(req, res, next) {
  Promise.all([
    req.payload ? User.findById(req.payload.id) : null,
    req.institute.populate("admin").execPopulate()
  ])
    .then(function(results) {
      var user = results[0];

      return res.json({ institute: req.institute.togeoJSONFor(user) });
    })
    .catch(next);
});

// update institute
router.put("/:institute", auth.required, function(req, res, next) {
  User.findById(req.payload.id).then(function(admin) {
    if (req.institute.admin._id.toString() === req.payload.id.toString()) {
      // admin: { type: mongoose.Types.ObjectId, ref: "User" },
      // category: String,
      // description: String,
      // county: String,
      // coordinates:
      if (typeof req.body.institute.name !== "undefined") {
        req.institute.name = req.body.institute.name;
      }
      if (typeof req.body.institute.category !== "undefined") {
        req.institute.category = req.body.institute.category;
      }
      if (typeof req.body.institute.description !== "undefined") {
        req.institute.description = req.body.institute.description;
      }
      if (typeof req.body.institute.coordinates !== "undefined") {
        req.institute.coordinates = req.body.institute.coordinates;
      }
      if (typeof req.body.institute.county !== "undefined") {
        req.institute.county = req.body.institute.county;
      }

      req.institute
        .save()
        .then(function(institute) {
          return res.json({ institute: institute.togeoJSONFor(admin) });
        })
        .catch(next);
    } else {
      return res.sendStatus(403);
    }
  });
});

// delete institute
router.delete("/:institute", auth.required, function(req, res, next) {
  User.findById(req.payload.id)
    .then(function(user) {
      if (!user) {
        return res.sendStatus(401);
      }

      if (req.institute.admin._id.toString() === req.payload.id.toString()) {
        return req.institute.remove().then(function() {
          return res.sendStatus(204);
        });
      } else {
        return res.sendStatus(403);
      }
    })
    .catch(next);
});
// return a institute's tasks
// router.get("/:institute/tasks", auth.optional, function(req, res, next) {
//   Promise.resolve(req.payload ? User.findById(req.payload.id) : null)
//     .then(function(user) {
//       return Promise.all([
//         Task.find({
//           institute: req.params.institute
//         })
//           .sort({ createdAt: "desc" })
//           .exec(),
//         Task.count({
//           institute: req.params.institute
//         }).exec()
//       ]).then(function(results) {
//         var tasks = results[0];
//         var tasksCount = results[1];
//         return res.json({
//           tasks: tasks.map(function(task) {
//             return task.toJSONFor();
//           }),
//           tasksCount: tasksCount
//         });
//       });
//     })
//     .catch(next);
// });
module.exports = router;
